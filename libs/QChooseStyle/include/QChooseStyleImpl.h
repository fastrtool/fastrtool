#ifndef QCHOOSESTYLEIMPL_H
#define QCHOOSESTYLEIMPL_H
//
#include <QDialog>
#include "ui_QChooseStyle.h"

#include <QStyleFactory>
#include <QSettings>
//
class QChooseStyleImpl : public QDialog, public Ui::QChooseStyle
{
	Q_OBJECT
	
public:
        QChooseStyleImpl( QWidget * parent = 0, Qt::WFlags f = 0 );
	
private slots:
	void on_keepButton_clicked();
	void on_tryButton_clicked();
	void on_quitButton_clicked();
	
private:
	QString beforeStyle;
};
#endif




