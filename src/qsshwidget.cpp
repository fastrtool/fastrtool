#include "include/qsshwidget.h"

#include <QInputDialog>
#include <QKeyEvent>
#include <QMessageBox>
#include <QScrollBar>

#include <QtDebug>

#include "include/qsshconnection.h"

QSshWidget::QSshWidget(QWidget *parent)
    :QPlainTextEdit(parent)
    ,m_sshConnection(NULL)
    ,m_cur(NULL)
    ,isNewLine(false)
{
   /***
    *   Creating the connection object and connecting signals & slots
    */
    m_sshConnection = new QSshConnection(this);
    if (!m_sshConnection) exit(-1);
    connect(m_sshConnection, SIGNAL(usernameRequested()), this, SLOT(askUsernameInDialog()), Qt::BlockingQueuedConnection);
    connect(m_sshConnection, SIGNAL(passwordRequested()), this, SLOT(askPasswordInDialog()), Qt::BlockingQueuedConnection);
    connect(m_sshConnection, SIGNAL(dataReceived(QByteArray)), this, SLOT(incomingData(QByteArray)), Qt::QueuedConnection);
    connect(m_sshConnection, SIGNAL(errorOccurred()), this, SLOT(errorOccured()), Qt::QueuedConnection);
    connect(m_sshConnection, SIGNAL(authFailed()), this, SLOT(authFailed()), Qt::QueuedConnection);
    connect(m_sshConnection, SIGNAL(finished()), this, SLOT(close()));

    setFont(QFont("Courier"));
    //setReadOnly(true);
}

QSshWidget::~QSshWidget()
{
    qDebug("Destructing a QSshWidget");
}

void QSshWidget::connectToHost(const QString &hostname, int port)
{
    m_sshConnection->connectToHost(hostname, port);
}

void QSshWidget::connectToHost(const QString &hostname, int port, const QString &username, const QString &password)
{
    m_sshConnection->connectToHost(hostname, port, username, password);
}

void QSshWidget::askUsernameInDialog()
{
    bool ok = false;
    QString username;
    do {
        if (ok) QMessageBox::information(this, trUtf8("Username - fastRtool"), trUtf8("Empty username not allowed"));
        username = QInputDialog::getText(this, trUtf8("Username - fastRtool"), trUtf8("Username :"), QLineEdit::Normal, QString(), &ok);
    } while (ok && username.isEmpty());

    if (ok)
        m_sshConnection->setUsername(username);
    else {
        QMessageBox::information(this, trUtf8("Username - fastRtool"), trUtf8("Connection cancelled by user"));
        m_sshConnection->endConnection();
    }
}

void QSshWidget::askPasswordInDialog()
{
    bool ok = false;
    QString password;
    do {
        if (ok) QMessageBox::information(this, trUtf8("Password - fastRtool"), trUtf8("Empty password not allowed"));
        password = QInputDialog::getText(this, trUtf8("Password - fastRtool"), trUtf8("Password :"), QLineEdit::PasswordEchoOnEdit, QString(), &ok);
    } while (ok && password.isEmpty());

    if (ok)
        m_sshConnection->setPassword(password);
    else {
        QMessageBox::information(this, trUtf8("Password - fastRtool"), trUtf8("Connection cancelled by user"));
        m_sshConnection->endConnection();
    }
}

void QSshWidget::incomingData(QByteArray data)
{
    QString reformatedText;

    if (data.isEmpty())
        return;

    QString debug = "";
    for (int i = 0; i < data.length(); ++i)
        debug.append(QString::number((unsigned char)data[i], 16) + " ");
    qDebug() << "Size of data " << data.length() << " : " << debug;
    reformatedText = QString::fromUtf8(data);

    // Simulating overwrite mode + analysing char
    qDebug() << reformatedText.length();
    const int len = reformatedText.length();
    for (int i = 0; i < len; ++i) {

        // Left arrow
        if (reformatedText.at(i) == 0x08)
            this->moveCursor(QTextCursor::Left);

        // Bell
        else if (reformatedText.at(i) == 0x07) {
            emit bellReceived();
            qDebug() << "Bell received";
        }

        // CR
        else if (reformatedText.at(i) == 0x0d) {
            this->moveCursor(QTextCursor::StartOfLine);
        }

        // LF
        else if (reformatedText.at(i) == 0x0a) {
            this->moveCursor(QTextCursor::EndOfLine);
            insertPlainText("\n");

            if (i > 0 && reformatedText.at(i-1) == 0x0d)
                isNewLine = true;
        }


        else if (reformatedText.at(i) == 0x1b) {    // 0x1b = ESC
            previous.append(0x1b);
        }


        else  if (reformatedText.at(i) == 0x5b && previous.size() == 1 && previous.at(0) == 0x1b) {   // 0x1b 0x5b = ESC [
            previous.append(0x5b);
        }

        else if (reformatedText.at(i) == 0x43 && previous.size() == 2 && previous.at(0) == 0x1b && previous.at(1) == 0x5b) {   // Right arrow (0x1b 0x5b 0x43 = ESC[C)
            this->moveCursor(QTextCursor::Right);
            previous.clear();
        }

        else if (reformatedText.at(i) == 0x4b && previous.size() == 2 && previous.at(0) == 0x1b && previous.at(1) == 0x5b) {  // Delete : erase all next chars (0x1b 0x5b 0x4b = ESC[K)
            this->moveCursor(QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
            this->textCursor().deleteChar();
            previous.clear();
        }

        // 0 - 9 : 0 for ESC[0m,  1 - 9 for ESC[1-9P or ESC[X;XXm or ESC[XX;XXm
          // 0x1b 0x5b 0x31 = ESC [ 1
        else if (reformatedText.at(i) >= 0x30 && reformatedText.at(i) <= 0x39 &&
                    (  previous.size() == 2 && previous.at(0) == 0x1b && previous.at(1) == 0x5b
                    || previous.size() == 3 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39
                    || previous.size() == 4 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) == 0x3b
                    || previous.size() == 5 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) >= 0x30 && previous.at(3) <= 0x39&& previous.at(4) == 0x3b
                    || previous.size() == 5 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) == 0x3b && previous.at(4) >= 0x30 && previous.at(4) <= 0x39
                    || previous.size() == 6 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) >= 0x30 && previous.at(3) <= 0x39&& previous.at(4) == 0x3b && previous.at(5) >= 0x30 && previous.at(5) <= 0x39
                    )
                 )
        {
            previous.append(reformatedText.at(i));
        }

        // Delete (erase next char) (0x1b 0x5b 0x31 0x50 = ESC[1P) and 2P 3P 4P = del 2 char, 3 char, 4 char? (always to the end of line?
        else if (reformatedText.at(i) == 0x50 && previous.size() == 3 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x31 && previous.at(2) <= 0x39) {
            for (int i = previous.at(2) - 0x30; i > 0; --i)
                this->textCursor().deleteChar();
            previous.clear();
        }

        // Remove colour bytes ESC[0m and ESC[XX;XXm and ESC[X;XXm
        else if (reformatedText.at(i) == 0x6d && previous.size() == 3 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) == 0x30) {  // ESC[0m
            previous.clear();
        }
        else if (reformatedText.at(i) == 0x3b &&
                     (  previous.size() == 3 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39   // ESC[X;XXm
                     || previous.size() == 4 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) >= 0x30 && previous.at(3) <= 0x39  // ESC[XX;XXm
                     )
                 )
        {
            previous.append(0x3b);
        }
        else if (reformatedText.at(i) == 0x6d &&
                    (  previous.size() == 6 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) >= 0x3b && previous.at(4) >= 0x30 && previous.at(4) <= 0x39 && previous.at(5) >= 0x30 && previous.at(5) <= 0x39// ESC[X;XXm
                    || previous.size() == 7 && previous.at(0) == 0x1b && previous.at(1) == 0x5b && previous.at(2) >= 0x30 && previous.at(2) <= 0x39 && previous.at(3) >= 0x30 && previous.at(3) <= 0x39 && previous.at(4) >= 0x3b && previous.at(5) >= 0x30 && previous.at(5) <= 0x39 && previous.at(6) >= 0x30 && previous.at(6) <= 0x39// ESC[XX;XXm
                    )
                 )
        {
            previous.clear();
        }

        else if (reformatedText.at(i) == 0x5d && previous.size() == 1 && previous.at(0) == 0x1b) {   // 0x1b 0x5d = ESC ]
            previous.append(0x5d);
        }

        // 0 - 9 : 0 for ESC]0m
        else if (reformatedText.at(i) >= 0x30 && reformatedText.at(i) <= 0x39  && previous.size() == 2 && previous.at(0) == 0x1b && previous.at(1) == 0x5d) {   // 0x1b 0x5d 0x3X = ESC ] X
            previous.append(reformatedText.at(i));
        }

        // TODO use previous instead of reading further
            // <OSC> number ; string <BEL>  == ESC]0;stringBEL
            else if (isNewLine && reformatedText.at(i) == 0x3b &&  previous.size() == 3 && previous.at(0) == 0x1b && previous.at(1) == 0x5d && previous.at(2) == 0x30) {
                i += readOSCmode(reformatedText.mid(i+1)) + 1;    // Will work, because the buffer will not cut it (it's always at the start of a reply, new line?)
                previous.clear();
            }

        else {
            this->textCursor().deleteChar();
            this->insertPlainText(reformatedText.at(i));
            isNewLine = false;
            previous.clear();
        }
    }

    // AutoScroll
    this->verticalScrollBar()->setValue(this->verticalScrollBar()->maximum());
}

int  QSshWidget::readOSCmode(const QString &data)
{
    QString title;
    int ret = data.indexOf(0x07);
    if (ret > 0) {
        title = data.left(ret);
        setWindowTitle(title);
    }
   // int ret = 3; // ESC already passed in the ++i of the for() and 3 for ] 0 ;

    // Read the string which is ended by a BEL

    return ret;
}

void QSshWidget::errorOccured()
{
    QMessageBox::critical(this, trUtf8("Error - fastRtool"), trUtf8("An error occurred :\n%1").arg(m_sshConnection->getLastError()));
}

void QSshWidget::authFailed()
{
    QMessageBox::critical(this, trUtf8("Authentication - fastRtool"), trUtf8("Authentication failed"));
}

void QSshWidget::askUserNameInline()
{
    appendPlainText("\nUsername : ");
}

void QSshWidget::askPasswordInline()
{
    appendPlainText("\nUsername : ");
}

void QSshWidget::keyPressEvent(QKeyEvent *ke)
{
    if (!m_sshConnection || m_sshConnection->getCurrentConnectionState() == QSshConnection::Disconnected) {
        ke->ignore();
        return;
    }

    qDebug("%d",ke->key());

    QByteArray ba;
    if (ke->modifiers() == Qt::NoModifier)
    {
        switch (ke->key())
        {
            case Qt::Key_Return:
            case Qt::Key_Enter: // CR
                ba.append(0x0d);
                break;
            case Qt::Key_Left:  // ESC[D
                ba.append(0x1b);
                ba.append(0x5b);
                ba.append(0x44);
                break;
            case Qt::Key_Right: // ESC[C
                ba.append(0x1b);
                ba.append(0x5b);
                ba.append(0x43);
                break;
            case Qt::Key_Up:
                ba.append(0x1b);
                ba.append(0x5b);
                ba.append(0x41);
                break;
            case Qt::Key_Down:
                ba.append(0x1b);
                ba.append(0x5b);
                ba.append(0x42);
                break;
            case Qt::Key_Delete:
                ba.append(0x04);
                break;
            case Qt::Key_Home:
                ba.append(0x01);
                break;
            case Qt::Key_End:
                ba.append(0x05);
                break;
            default:
                ba.append(ke->text().toUtf8().constData());
        }
        m_sshConnection->sendData(ba);
    }
    else if (ke->modifiers() & Qt::ControlModifier)
    {
        switch (ke->key())
        {
            default:
                m_sshConnection->sendData(qPrintable(ke->text()));
        }
    }
    else
        // Send Event I didn't know ... for the moment !
        //QPlainTextEdit::keyPressEvent(ke);
        m_sshConnection->sendData(qPrintable(ke->text()));
}

QSize QSshWidget::sizeHint() const
{
    QFontMetrics fm = fontMetrics();

    // 80 * 24
    int w = fm.averageCharWidth() * 80;
    int h = fm.height() * 24;

    //w += fm.c
    h += fm.leading() * 25; // (23 + 2)

    return QSize(w, h);
}
/*
void QSshWidget::mousePressEvent(QMouseEvent *me)
{
    if (m_cur)
        delete m_cur;

    m_cur = new QTextCursor(this->textCursor());
}

void QSshWidget::mouseReleaseEvent(QMouseEvent *me)
{
    if (m_cur) {
        this->setTextCursor(*m_cur);
        delete m_cur;
        m_cur = NULL;
    }
}

void QSshWidget::mouseDoubleClickEvent(QMouseEvent *me)
{

}
*/
