#include "include/qsshconnection.h"

QSshConnection::QSshConnection(QObject *parent)
    :QThread(parent)
    ,m_connectionState(Disconnected)
{

}

QSshConnection::~QSshConnection()
{
    m_mutex.lock();
    m_connectionState = Disconnected;
    m_waitCond.wakeOne();
    m_mutex.unlock();
    wait();
}

bool QSshConnection::isConnected() const
{
    //return ssh_is_connected(sshSession) == 1;
    return true;
}

bool QSshConnection::connectToHost(const QString &hostname, quint16 port, const QString &username, const QString &password)
{
    QMutexLocker locker(&m_mutex);
    m_hostname = hostname;
    m_port = port;
    m_username = username;
    m_password = password;
    if (!isRunning()) {
        start();
        return true;
    } else {
        qDebug("Thread is busy (%d)", m_connectionState);
        return false;
    }
}

QString QSshConnection::getLastError() const
{
    QMutexLocker locker(&m_mutex);
    return m_lastError;
}

QSshConnection::State QSshConnection::getCurrentConnectionState() const
{
    QMutexLocker locker(&m_mutex);
    return m_connectionState;
}

void QSshConnection::setUsername(const QString &username)
{
    QMutexLocker locker(&m_mutex);
    m_username = username;
}

void QSshConnection::setPassword(const QString &password)
{
    QMutexLocker locker(&m_mutex);
    m_password = password;
}

void QSshConnection::sendData(const QByteArray &data)
{
    QMutexLocker locker(&m_mutex);
    m_dataToSend.append(QString::fromUtf8(data));
}

void QSshConnection::endConnection()
{
    QMutexLocker locker(&m_mutex);
    m_connectionState = Disconnected;
    m_waitCond.wakeOne();
}

void QSshConnection::run()
{
    m_mutex.lock();
    QString hostname = m_hostname;
    quint16 port = m_port;
    QString username = m_username;
    QString password = m_password;
    State status = m_connectionState = CreatingSshSession;
    m_mutex.unlock();

   /***
    *   Creating a ssh session
    */
    ssh_session sshSession = ssh_new();
    if (sshSession == NULL) {
        m_lastError = "Can't allocate a new ssh_session";
        emit errorOccurred();
        return;
    }

   /***
    *   Set options on this session
    */
    ssh_options_set(sshSession, SSH_OPTIONS_HOST, hostname.toLatin1().constData());
    ssh_options_set(sshSession, SSH_OPTIONS_PORT, &port);

    m_mutex.lock();
    status = m_connectionState = Connecting;
    m_mutex.unlock();

   /***
    *   Connecting to the  server
    */
    if (ssh_connect(sshSession) != SSH_OK) {
        m_lastError = QString("Connection failed (%1)").arg(ssh_get_error(sshSession));
        emit errorOccurred();
        ssh_free(sshSession);
        return;
    }

   /***
    *   Ask the username
    */
    while (status == Connecting && username.isEmpty()) {
        emit usernameRequested();   // Must be blocking by using Qt::BlockingQueuedConnection
        m_mutex.lock();
        username = m_username;
        status = m_connectionState;
        m_mutex.unlock();
    }

   /***
    *   Ask the password
    */
    while (status == Connecting && password.isEmpty()) {
        emit passwordRequested();   // Must be blocking by using Qt::BlockingQueuedConnection
        m_mutex.lock();
        password = m_password;
        status = m_connectionState;
        m_mutex.unlock();
    }

    if (status == Disconnected) {
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;
    }

    m_mutex.lock();
    status = m_connectionState = Authenticating;
    m_mutex.unlock();

   /***
    *   Try to authenticate to the server
    */
    if (ssh_userauth_password(sshSession, qPrintable(username), qPrintable(password)) != SSH_AUTH_SUCCESS) {
        m_lastError = QString("Authentication failed (%1)").arg(ssh_get_error(sshSession));
        emit authFailed();
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;
    }

    m_mutex.lock();
    status = m_connectionState = CreatingChannel;
    m_mutex.unlock();

   /***
    *   Creating a channel on the previously created ssh session
    */
    ssh_channel sshChannel = ssh_channel_new(sshSession);
    if (sshChannel == NULL) {
        m_lastError = "Can't allocate a new ssh_channel";
        emit errorOccurred();
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;
    }

    m_mutex.lock();
    status = m_connectionState = OpenningChannelSession;
    m_mutex.unlock();

   /***
    *   Open a session on this channel
    */
    if (ssh_channel_open_session(sshChannel) != SSH_OK) {
        m_lastError = "Can't open a session on the channel";
        emit errorOccurred();
        ssh_channel_free(sshChannel);
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;
    }

    m_mutex.lock();
    status = m_connectionState = ConfiguringPty;
    m_mutex.unlock();

   /***
    *   Configuring a remote pty
    */
    if (ssh_channel_request_pty(sshChannel) != SSH_OK) {
        m_lastError = "Can't request pty on the channel";
        emit errorOccurred();
        ssh_channel_close(sshChannel);
        ssh_channel_send_eof(sshChannel);
        ssh_channel_free(sshChannel);
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;
    }

    if (ssh_channel_change_pty_size(sshChannel, 80, 24) != SSH_OK) {
        m_lastError = "Can't change the pty size";
        emit errorOccurred();
        ssh_channel_close(sshChannel);
        ssh_channel_send_eof(sshChannel);
        ssh_channel_free(sshChannel);
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;  // TODO may not return because it is not a serious error
    }

    if (ssh_channel_request_shell(sshChannel) != SSH_OK) {
        m_lastError = "Can't request a shell";
        emit errorOccurred();
        ssh_channel_close(sshChannel);
        ssh_channel_send_eof(sshChannel);
        ssh_channel_free(sshChannel);
        ssh_disconnect(sshSession);
        ssh_free(sshSession);
        return;  // TODO may not return because it is not a serious error
    }

    m_mutex.lock();
    status = m_connectionState = Connected;
    m_mutex.unlock();

   /***
    *   Read and Write data
    */
    int nbytes;
    char buffer[256];
    QString test;
    QString dataToSend;
    while (status == Connected &&
           ssh_channel_is_open(sshChannel) &&
           !ssh_channel_is_eof(sshChannel)) {

        memset(buffer, 0, sizeof(buffer));

        // READING
        nbytes = ssh_channel_read_nonblocking(sshChannel, buffer, sizeof(buffer), 0);
        if (nbytes > 0) {
            //qDebug("%i bytes received", nbytes);
            emit dataReceived(QByteArray(buffer));
        }

        // SENDING
        m_mutex.lock();
        status = m_connectionState;

        if (m_dataToSend.isEmpty()) {
            m_mutex.unlock();
            msleep(100);
        } else {
            dataToSend = m_dataToSend;
            m_dataToSend.clear();
            m_mutex.unlock();
            nbytes = ssh_channel_write(sshChannel, dataToSend.toUtf8().constData(), strlen(dataToSend.toUtf8().constData()));
            //qDebug("%i bytes writted", nbytes);
        }
    }

    // End the connection
    m_mutex.lock();
    m_connectionState = Disconnected;
    m_mutex.unlock();
    ssh_channel_close(sshChannel);
    ssh_channel_send_eof(sshChannel);
    ssh_channel_free(sshChannel);
    ssh_disconnect(sshSession);
    ssh_free(sshSession);
}
