#include "include/mainwindow.h"

#include <QInputDialog>
#include <QMessageBox>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QSettings>
#include <QStackedWidget>
#include <QStyleFactory>

#include "libs/QChooseStyle/include/QChooseStyleImpl.h"
#include "include/qsshwidget.h"

MainWindow::MainWindow(QWidget *parent)
    :QMainWindow(parent)
{
    setupUi(this);

   /***
    *   Initialising the menu
    */
    connect(action_New, SIGNAL(triggered()), this, SLOT(newConnection()));
    connect(action_Style, SIGNAL(triggered()), this, SLOT(chooseStyle()));
    connect(action_AboutQt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(action_Exit, SIGNAL(triggered()), this, SLOT(close()) );


   /***
    *   Load preffered GUI Style
    */
    QSettings settings;

    QString s  = settings.value("style").toString();
    if (!s.isEmpty())
        QApplication::setStyle(QStyleFactory::create(s));

   /***
    *   Create and populate the stack widget
    */
}

MainWindow::~MainWindow()
{
}

void MainWindow::chooseStyle()
{
    QChooseStyleImpl style;
    style.exec();
}

void MainWindow::newConnection()
{
    QSshWidget *sshWidget = new QSshWidget();
    QMdiSubWindow *subWindow = mdiArea->addSubWindow(sshWidget);
    connect(sshWidget, SIGNAL(destroyed()), subWindow, SLOT(close()));  // Don't work (killing parent when child killed)
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    subWindow->show();
    sshWidget->connectToHost("10.161.0.100", 22, "redkite", "chnt");
}
