#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

#include "ui_mainwindow.h"

namespace Ui
{
    class MainWindowClass;
}

class MainWindow : public QMainWindow, public Ui::MainWindowClass
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void chooseStyle();
    void newConnection();

private:
    //quint32             m_nextConnectionID;
    //QMap<QSshObject *>  m_sshConnections;
};

#endif // MAINWINDOW_H
