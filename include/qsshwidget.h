#ifndef QSSHWIDGET_H
#define QSSHWIDGET_H

#include <QPlainTextEdit>
//#include <QSize>

class QKeyEvent;
class QTextCursor;
class QSshConnection;

class QSshWidget : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit QSshWidget(QWidget *parent=0);
    ~QSshWidget();

    void connectToHost(const QString &hostname, int port=22);
    void connectToHost(const QString &hostname, int port, const QString &username, const QString &password);

    virtual QSize sizeHint() const;
/*
    void mousePressEvent(QMouseEvent *me);
    void mouseReleaseEvent(QMouseEvent *me);
    void mouseDoubleClickEvent(QMouseEvent *me);
*/
public slots:
    void askUserNameInline();
    void askUsernameInDialog();
    void askPasswordInline();
    void askPasswordInDialog();
    void incomingData(QByteArray);
    void errorOccured();
    void authFailed();

signals:
    void bellReceived();

protected:
    void keyPressEvent(QKeyEvent *e);

private:
    QSshConnection *m_sshConnection;

    QTextCursor *m_cur;

    bool isNewLine;
    int readOSCmode(const QString &);
    QByteArray previous;
};

#endif  // QSSHWIDGET_H
