#ifndef QSSHCONNECTION_H
#define QSSHCONNECTION_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>

#include "libssh/libssh.h"

class QxtSignalWaiter;

class QSshConnection : public QThread
{
    Q_OBJECT

public:    
    enum State {
        Disconnected,
        CreatingSshSession,
        Connecting,
        Authenticating,
        CreatingChannel,
        OpenningChannelSession,
        ConfiguringPty,
        Connected,
    };

    explicit QSshConnection(QObject *parent = 0);
    ~QSshConnection();

    bool connectToHost(const QString &hostname, quint16 port=22, const QString &username=QString(), const QString &password=QString());
    bool isConnected() const;
    void endConnection();

    QString         getLastError() const;
    State getCurrentConnectionState() const;

    void run();

    int parsePlaintext(const QByteArray &data);
public slots:
    void setUsername(const QString &);
    void setPassword(const QString &);
    void sendData(const QByteArray &);

signals:
    void connected();
    void disconnected();
    void dataReceived(QByteArray);
    void errorOccurred();
    void usernameRequested();
    void passwordRequested();
    void authFailed();

private:
    QString         m_lastError;

    mutable QMutex  m_mutex;
    QWaitCondition  m_waitCond;
    State           m_connectionState;
    QString         m_hostname;
    quint16         m_port;
    QString         m_username;
    QString         m_password;
    QByteArray      m_dataToSend;

};

#endif  // QSSHCONNECTION_H
