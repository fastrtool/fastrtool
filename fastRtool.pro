#-------------------------------------------------
#
# Project created by QtCreator 2011-04-13T22:42:23
#
#-------------------------------------------------

QT       += core gui network
LIBS     += -lssh

TARGET = fastRtool
TEMPLATE = app

SOURCES += \
    libs/QChooseStyle/src/QChooseStyleImpl.cpp \
    libs/QLedButton/src/qled.cpp \
    libs/QLedButton/src/qledbutton.cpp \
    src/mainwindow.cpp \
    src/main.cpp \
    src/qsshconnection.cpp \
    src/qsshwidget.cpp
HEADERS  += \
    libs/QChooseStyle/include/QChooseStyleImpl.h \
    libs/QLedButton/include/qled.h \
    libs/QLedButton/include/qledbutton.h \
    include/mainwindow.h \
    include/qsshconnection.h \
    include/qsshwidget.h
FORMS    += \
    libs/QChooseStyle/ui/QChooseStyle.ui \
    ui/mainwindow.ui
